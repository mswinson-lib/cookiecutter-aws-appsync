# cookiecutter-aws-appsync

  generate a project for building AWS AppSync Schemas

## Prerequisites

   python  
   cookiecutter  

## Usage

    cookiecutter bb:mswinson-lib/cookiecutter-aws-appsync.git


## License

This project is licensed under the terms of the [MIT License](/LICENSE)
