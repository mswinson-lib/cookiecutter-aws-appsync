require 'yaml'
require 'erb'

# yaml map
class YAMLMap < Hash
  # returns resource data
  # @return [Hash]
  def resource
    h = {}
    each do |key,value|
      next unless value.respond_to?(:resource)
      h[key] = value.resource
    end
    h
  end

  # return outputs
  # @return [Hash]
  def outputs
    h = {}
    each do |key,value|
      next unless value.respond_to?(:outputs)
      outputs = value.outputs
      h.merge!(outputs)
    end
    h
  end
end


class YAMLResource
  attr_reader :name
  attr_reader :data

  # create yaml resource(s) from path
  # @param path [String] filesystem path
  # @return [YAMLResource] if path is a file
  # @return [YAMLMap] if path is a directory
  def self.from_path(path)
    return from_dir(path) if File.directory?(path)
    return from_file(path) if File.file?(path)
  end

  # create yaml map from path
  # @param path [String] directory
  # @return [YAMLMap]
  def self.from_dir(path)
    resources = YAMLMap.new

    Dir.chdir(path) do
      Dir.glob('./**/*.yml').each do |f|
        resource = YAMLResource.from_file(f)
        resources[resource.name] = resource
      end
    end
    resources
  end

  # create yaml resource from path
  # @param path [String] filesystem path
  # @return [YAMLResource]
  def self.from_file(path)
    name = path.gsub('./','').gsub('.yml','').gsub('/','_')

    b = binding
    source = File.read(path)
    yaml_data = {}

    Dir.chdir(File.dirname(path)) do
      t = ERB.new(source, nil, '-')
      yaml_data = t.result(b)
    end

    data = YAML.load(yaml_data)
    new(name, data)
  end

  # return output data
  # @return [Hash]
  def outputs
    @data['outputs'] || {}
  end

  # return resource data
  # @return [Hash]
  def resource
    @data['resource'] || {}
  end

  private

  # initialize resource
  # @api private
  # @param name [String] resource name
  # @param data [Hash] resource data
  def initialize(name, data)
    @name = name
    @data = data
  end
end
